<?php

namespace Drupal\otp_auth;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\Entity\User;
use Drupal\sms\Entity\SmsMessage;
use Drupal\sms\Entity\SmsGateway;
use Drupal\sms\Direction;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sms\Provider\SmsProviderInterface;
use Drupal\user\UserDataInterface;
use function GuzzleHttp\Psr7\str;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class Otp.
 */
class Otp {

  /**
   * A date time instance.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $currentTime;

  /**
   * The SMS Provider.
   *
   * @var \Drupal\sms\Provider\SmsProviderInterface
   */
  protected $smsProvider;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Creates an object.
   *
   * @param Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\sms\Provider\SmsProviderInterface $sms_provider
   *   The SMS service provider.
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   */
  public function __construct(TimeInterface $time, SmsProviderInterface $sms_provider, UserDataInterface $user_data, EntityTypeManagerInterface $entity_type_manager, ConfigFactory $config_factory) {
    $this->currentTime = $time;
    $this->smsProvider = $sms_provider;
    $this->userData = $user_data;
    $this->entityTypeManager = $entity_type_manager;
    $this->config_factory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('datetime.time'),
      $container->get('sms.provider'),
      $container->get('user.data'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * 
   */
  public function sendOtp($code, $mobile_number, $gateway) {

    $sms = SmsMessage::create()
      ->setMessage((string) t('Your OTP is: :code', [':code' => $code]))
      ->addRecipient($mobile_number)
      ->setAutomated(true);

    $sms->setSendTime((new DrupalDateTime('now'))->format('U'));
    $sms->setDirection(Direction::OUTGOING);
    $sms->setGateway($gateway);
    $messages = $this->smsProvider->send($sms);

    foreach ($messages as $message) {
      $result = $message->getResult();
      if ($result->getError()) {
        return false;
      }
    }

    return $gateway->id();
  }

  /**
   * {@inheritdoc}
   */
  public function generateOtp($mobile_number, $ignore_method = null) {

    // Get current user
    $user = $this->OtpAuthCheckUserAlreadyExists($mobile_number);
    if (!$user) {
      // Create user object.
      $host = preg_replace('#^www\.#', '', \Drupal::request()->getHost());
      $account = User::create();
      $account->set("name", $mobile_number);
      $account->set("phone_number", $mobile_number);
      $account->set("mail", "{$mobile_number}@{$host}");
      $account->save();
      $user = $this->OtpAuthCheckUserAlreadyExists($mobile_number);
    }

    $uid = $user->id();
    $data = $this->userData->get('otp_auth', $uid, 'otp_user_data');

    // Generate 6 digit random OTP number.
    $six_digit_random_number = mt_rand(100000, 999999);

    // Send OTP SMS.
    $gateways = SmsGateway::loadMultiple();
    foreach ($gateways as $sms_gateway) {
      if (count($gateways) > 1 && !empty($ignore_method) && $sms_gateway->id() == $ignore_method) {
        continue;
      }

      if ($send_method = $this->sendOtp($six_digit_random_number, $mobile_number, $sms_gateway)) {
        break;
      }
    }

    if (!$send_method) {
      return false;
    }

    $current_time = $this->currentTime->getCurrentTime();
    $sessions = $data['sessions'];
    if ($data['otps'] && $otps = array_keys($data['otps'])) {
      $last_otp_key = end($otps);
      $new_otp_key = $last_otp_key + 1;
      $otps = array_merge($data['otps'], [$new_otp_key => ['otp' => $six_digit_random_number, 'otp_time' => $current_time]]);
    }
    else {
      $otps = [['otp' => $six_digit_random_number, 'otp_time' => $current_time]];
    }
    $otp_user_data = [
      "mobile_number" => $mobile_number,
      "otps" => $otps,
      "last_otp_time" => $current_time,
      "sessions" => $sessions,
    ];
    $this->userData->set('otp_auth', $uid, 'otp_user_data', $otp_user_data);

    return $send_method;
  }

  /**
   * {@inheritdoc}
   */
  public function OtpAuthCheckUserAlreadyExists($phone_number) {
    if (empty($phone_number)) {
      return;
    }

    $accounts = $this->entityTypeManager->getStorage('user')->loadByProperties(['name' => $phone_number]);
    $account = reset($accounts);
    return $account;
  }

  /**
   * {@inheritdoc}
   */
  public function validateOtp($otp, $mobile_number) {
    $users = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['name' => $mobile_number]);
    $user = reset($users);
    if ($user) {
      $uid = $user->id();
    }
    // Get OTP from database.
    $data = $this->userData->get('otp_auth', $uid, 'otp_user_data');
    if (!empty($mobile_number) && !empty($otp)) {
      if (array_search($otp, array_column($data['otps'], 'otp')) === FALSE) {
        $otp_incorrect = TRUE;
        return $otp_incorrect;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function userOtpAuth($otp, $mobile_number) {
    $user = $this->OtpAuthCheckUserAlreadyExists($mobile_number);
    $uid = $user->id();
    // Generate 6 digit random session id.
    $six_digit_random_sessionid = mt_rand(100000, 999999);

    // Activate user account if not activated.
    if (!$user->isActive()) {
      $user->set("status", 1);
      $user->save();
    }
    $data = $this->userData->get('otp_auth', $uid, 'otp_user_data');
    $lastotptime = $data['last_otp_time'];
    if (($key = array_search($otp, array_column($data['otps'], 'otp'))) !== FALSE) {
      unset($data['otps'][$key]);
      $otps = array_values($data['otps']);
    }
    if ($data['sessions']) {
      $sessions = $data['sessions'];
      $sessions[] = $six_digit_random_sessionid;
    }
    else {
      $sessions = [$six_digit_random_sessionid];
    }
    $session_user_data = [
      "mobile_number" => $mobile_number,
      "otps" => $otps,
      "last_otp_time" => $lastotptime,
      "sessions" => $sessions,
    ];
    $this->userData->set('otp_auth', $uid, 'otp_user_data', $session_user_data);

    // Login programmatically as a user.
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    user_login_finalize($user);

    return $six_digit_random_sessionid;
  }

  /**
   * {@inheritdoc}
   */
  public function validateSessionid($session_id, $mobile_number) {
    $users = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['name' => $mobile_number]);
    $user = reset($users);
    if ($user) {
      $uid = $user->id();
    }
    // Get sessions from database.
    $data = $this->userData->get('otp_auth', $uid, 'otp_user_data');

    if (!empty($mobile_number) && !empty($session_id)) {
      if (!in_array($session_id, $data['sessions'])) {
        $session_id_incorrect = TRUE;
        return $session_id_incorrect;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function userOtpLogout($session_id, $mobile_number) {
    $users = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['name' => $mobile_number]);
    $user = reset($users);
    if ($user) {
      $uid = $user->id();
    }
    $data = $this->userData->get('otp_auth', $uid, 'otp_user_data');
    $lastotptime = $data['last_otp_time'];
    if (($key = array_search($session_id, $data['sessions'])) !== FALSE) {
      unset($data['sessions'][$key]);
      $sessions = array_values($data['sessions']);
    }
    else {
      $sessions = $data['sessions'];
    }
    $session_user_data = [
      "mobile_number" => $mobile_number,
      "otps" => $data['otps'],
      "last_otp_time" => $lastotptime,
      "sessions" => $sessions,
    ];
    $this->userData->set('otp_auth', $uid, 'otp_user_data', $session_user_data);
  }

}
