<?php

namespace Drupal\otp_auth\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {
  
  /**
  * The config factory.
  *
  * @var \Drupal\Core\Config\ConfigFactoryInterface
  */
  protected $configFactory;

  /**
   * Constructs a new NodeAdminRouteSubscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    if ($this->configFactory->get('otp_auth.settings')->get('activate')) {
      $paths = [
        'user.login',
        'user.register',
        'user.pass',
      ];

      // Change path to 'user/auth'.
      foreach ($paths as $path) {
        if ($route = $collection->get($path)) {
          $route->setPath('user/auth');
        }
      }
    }
  }

}