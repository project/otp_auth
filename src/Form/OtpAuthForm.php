<?php

namespace Drupal\otp_auth\Form;

use Drupal\otp_auth\Otp;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function GuzzleHttp\Psr7\str;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class OTPAuthForm.
 */
class OtpAuthForm extends FormBase {

  /**
   * The otp service.
   *
   * @var \Drupal\otp_auth\Otp
   */
  protected $OTP;

  protected $cipherMethod;
  protected $separator;
  protected $ivLength;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('otp_auth.OTP')
    );
  }

  /**
   * Creates an object.
   *
   * @param Drupal\otp_auth\Otp $OTPAuth
   *   The otp service.
   */
  public function __construct(Otp $OTPAuth) {
    $this->OTP = $OTPAuth;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'otp_auth_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $user_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    if (!isset($user_fields['phone_number'])) {
      \Drupal::messenger()->addWarning(t('Please call with website admin.'));
      return $form;
    }

    if (empty($form_state->get('step'))) {
      $form_state->set('step', 'phone');
    }

    if ($form_state->get('step') == 'phone') {
      $sample = $this->config('otp_auth.settings')->get('phone_sample');
      $form['mobile_number'] = [
        '#type' => 'tel',
        '#title' => $this->t('Mobile Number'),
        '#description' => $this->t('Enter valid mobile number.'),
        '#required' => TRUE,
        '#weight' => '0',
        '#pattern' => trim($this->config('otp_auth.settings')->get('phone_regex'), '/'),
        '#attributes' => [
          'class' => ['phone_intl'],
          'autocomplete' => 'off',
          'placeholder' => !empty($sample) ? $this->t('Like as @sample', ['@sample' => $sample]) : '',
        ],
        '#attached' => [
          'library' => ['otp_auth/intl-phone'],
          'drupalSettings' => ['otp_auth' => ['phone_country' => $this->config('otp_auth.settings')->get('phone_country')]],
        ],
      ];
      $form['generate_otp'] = [
        '#type' => 'submit',
        '#value' => $this->t('Continue'),
        '#weight' => '0',
      ];
    }

    if ($form_state->get('step') == 'verify') {
      $form['otp_validate'] = [
        '#type' => 'fieldset',
        '#prefix' => '<div id="otpValidateWrapper">',
        '#suffix' => '</div>',
      ];

      $form['otp_validate']['otp_code'] = [
        '#type' => 'textfield',
        '#title' => $this->t('OTP Code'),
        '#description' => $this->t('Enter received code.'),
        '#required' => TRUE,
        '#maxlength' => 8,
        '#size' => 8,
        '#weight' => '1',
        '#attributes' => [
          'autocomplete' => 'off',
        ],
        '#attached' => [
          'library' => ['otp_auth/otp-auth'],
          'drupalSettings' => ['otp_auth' => ['resend_wait_time' => $this->config('otp_auth.settings')->get('resend_wait_time')]],
        ],
      ];

      $form['otp_validate']['retry'] = [
        '#type' => 'submit',
        '#value' => $this->t('Resend code'),
        '#submit' => ['::retryForm'],
        '#limit_validation_errors' => [],
        '#attributes' => [
          'class' => ['user-retry-verification'],
        ],
        '#weight' => '2',
      ];

      $form['otp_validate']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Login'),
        '#weight' => '3',
      ];

      $form['otp_validate']['back'] = [
        '#type' => 'submit',
        '#value' => $this->t('Back'),
        '#submit' => ['::cancelForm'],
        '#limit_validation_errors' => [],
        '#weight' => '4',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function OTPAuthGenerateOtp(array $form, FormStateInterface $form_state) {
    $otp_platform = $this->config('otp_auth.settings')->get('otp_platform');
    $mobile_number = $form_state->getValue('mobile_number');

    if ($otp_platform == 'sms') {
      if ($method = $this->OTP->generateOtp($mobile_number)) {
        $form_state->set('phone', $mobile_number);
        $form_state->set('step', 'verify');
        $form_state->set('otp_send', TRUE);
        $form_state->set('sms_method', $method);
      }
      else {
        \Drupal::messenger()->addMessage("Can't send SMS now.", 'warning');
      }
    }

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function retryForm(array &$form, FormStateInterface $form_state) {
    $otp_platform = $this->config('otp_auth.settings')->get('otp_platform');
    $resend_wait_time = $this->config('otp_auth.settings')->get('resend_wait_time');
    $resend_wait_time = empty($resend_wait_time) ? 120 : $resend_wait_time;

    if ($otp_platform == 'sms') {

      $user = $this->OTP->OtpAuthCheckUserAlreadyExists($form_state->get('phone'));
      $data = $this->OTP->userData->get('otp_auth', $user->id(), 'otp_user_data');

      // Generate OTP after wait time
      if (!empty($data['last_otp_time'])
        && $data['last_otp_time'] > $this->OTP->currentTime->getCurrentTime() - $resend_wait_time) {
          \Drupal::messenger()->addMessage("Can't resend OTP now, please wait.", 'warning');
      }
      else {
        if ($method = $this->OTP->generateOtp($form_state->get('phone'), $form_state->get('sms_method'))) {
          $form_state->set('step', 'verify');
          $form_state->set('otp_send', TRUE);
          $form_state->set('sms_method', $method);
        }
        else {
          \Drupal::messenger()->addMessage("Can't send SMS now.", 'warning');
        }
      }
    }

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $url = Url::fromRoute('otp_auth.otp_auth_form');
    $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->get('step') == 'phone') {

      if (!preg_match($this->config('otp_auth.settings')->get('phone_regex'), $form_state->getValue('mobile_number'))) {
        $form_state->setErrorByName('mobile_number', $this->t('Invalid mobile number. Like as: 9123456789 without zero.'));
      }

      $user = $this->OTP->OTPAuthCheckUserAlreadyExists($form_state->getValue('mobile_number'));
      if ($user && $user->hasRole('administrator')){
        $form_state->setErrorByName('mobile_number', $this->t('Admin users can\'t login by OTP.'));
      }

      return;
    }

    $config = $this->config('otp_auth.settings');
    $otp_platform = $config->get('otp_platform');
    $otp = $form_state->getValue('otp_code');
    $mobile_number = $form_state->get('phone');

    if ($otp_platform == 'sms') {
      if ($this->OTP->validateOtp($otp, $mobile_number)) {
        $form_state->setErrorByName('otp_code', $this->t('Incorrect OTP. Please provide correct OTP.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->get('step') == 'phone') {
      $this->OTPAuthGenerateOtp($form, $form_state);

      return;
    }

    $otp = $form_state->getValue('otp_code');
    $mobile_number = $form_state->get('phone');
    $user = $this->OTP->OTPAuthCheckUserAlreadyExists($mobile_number);

    if ($user) {
      $session_id = $this->OTP->userOTPAuth($otp, $mobile_number);
      // Save user cookie.
      user_cookie_save([$mobile_number => $session_id]);
      // Redirect to user profile page.
      $url = Url::fromRoute('user.page');
      $form_state->setRedirectUrl($url);
    }
  }
}
