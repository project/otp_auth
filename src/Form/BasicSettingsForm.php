<?php

namespace Drupal\otp_auth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form builder for the otp_auth basic settings form.
 */
class BasicSettingsForm extends ConfigFormBase {

  protected $cipherMethod;
  protected $separator;
  protected $ivLength;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->cipherMethod = 'AES-256-CBC';
    $this->separator = '::';
    $this->ivLength = openssl_cipher_iv_length($this->cipherMethod);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'otp_auth_basic_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['otp_auth.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('otp_auth.settings');

    $form['basic'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Basic settings'),
      '#collapsible' => FALSE,
    ];

    $form['basic']['activate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate OTP Auth'),
      '#default_value' => $config->get('activate'),
      '#description' => $this->t('Activate authentication via OTP Auth.'),
    ];

    $form['otp_type'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Choose OTP platform'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['otp_type']['otp_platform'] = [
      '#type' => 'radios',
      '#title' => $this->t('With SMS framework you can use any supported gateway to send OTP.'),
      '#default_value' => $config->get('otp_platform'),
      '#options' => [
        'sms' => $this->t('SMS Framework'),
      ],
    ];

    $form['purge_user'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Purge users who have been blocked / not logged-in for'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['purge_user']['user_blocked_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Days'),
      '#description' => $this->t('Enter value in days.'),
      '#default_value' => $config->get('user_blocked_value'),
    ];

    $form['purge_user']['enabled_blocked_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled_blocked_users'),
    ];

    $form['otp_expire'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Enter time after which OTP should expire.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['otp_expire']['otp_expire_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minutes'),
      '#description' => $this->t('Enter value in minutes.'),
      '#default_value' => $config->get('otp_expire_value'),
    ];

    $form['phone'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Phone settings'),
      '#collapsible' => FALSE,
    ];

    $form['phone']['phone_regex'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Regex'),
      '#default_value' => $config->get('phone_regex') ?? "/^9[0-9]{9}$/",
      '#description' => $this->t('Regex for validate phone number.'),
    ];

    $form['phone']['phone_country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country'),
      '#default_value' => $config->get('phone_country'),
      '#description' => $this->t('If you want to accept a specific country(us, fr, uk, ir, ...), set it but if leave empty, will accept all countries. For country codes: https://intl-tel-input.com/'),
    ];

    $form['phone']['phone_sample'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sample'),
      '#default_value' => $config->get('phone_sample'),
      '#description' => $this->t('Phone sample for use in field placeholder.'),
    ];

    $form['resend'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Resend settings'),
      '#collapsible' => FALSE,
    ];

    $form['resend']['resend_wait_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resend wait time (secondes)'),
      '#default_value' => $config->get('resend_wait_time') ?? 120,
      '#description' => $this->t('Wait time before resend OTP.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('otp_auth.settings');
    $config->set('activate', $form_state->getValue('activate'));
    $config->set('otp_platform', $form_state->getValue('otp_platform'));
    $config->set('user_blocked_value', $form_state->getValue('user_blocked_value'));
    $config->set('enabled_blocked_users', $form_state->getValue('enabled_blocked_users'));
    $config->set('otp_expire_value', $form_state->getValue('otp_expire_value'));
    $config->set('phone_regex', $form_state->getValue('phone_regex'));
    $config->set('phone_country', $form_state->getValue('phone_country'));
    $config->set('phone_sample', $form_state->getValue('phone_sample'));
    $config->set('resend_wait_time', $form_state->getValue('resend_wait_time'));
    $config->save();

    // Rebuild caches
    drupal_flush_all_caches();
  }

}
