<?php

namespace Drupal\otp_auth\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AuthSubscriber implements EventSubscriberInterface {

  /**
   * @param GetResponseEvent $event
   */
  public function checkForRedirection(GetResponseEvent $event) {

    $current_path = \Drupal::service('path.current')->getPath();

    $paths = [
      '/user/login',
      '/user/register',
      '/user/reset',
    ];

    // Change path to 'user/auth'.
    if (in_array($current_path, $paths)) {
      $event->setResponse(new RedirectResponse(\Drupal::url('otp_auth.otp_auth_form', [], ['absolute' => TRUE])));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForRedirection'];
    return $events;
  }

}