(function (Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.PhoneInternational = {
    attach: function (context, settings) {
      // Do something like jquery.once. Be sure that this attach only runs once.
      var fields = document.querySelectorAll('.phone_intl');
      if (fields.length) {
        if (!document.querySelector(".jsIntPhone")) {
          document.querySelector('.phone_intl').classList.add('jsIntPhone');

          // Loop each one and load the library.
          fields.forEach(function(field) {
            // As we are using attach form, check first if its already loaded.
            var parent = field.parentElement;
            if (!parent.classList.contains('intl-tel-input')) {
              // Find the field that writes the code.
              var field_code = document.querySelector('[name="' + field.name + '"]');
              // Initialize the phone library.
              var intlTelInputConfig = {
                initialCountry: '',
                preferredCountries: [],
                allowDropdown: true,
              }
              if (settings.otp_auth.phone_country.length == 2) {
                intlTelInputConfig = {
                  initialCountry: settings.otp_auth.phone_country,
                  preferredCountries: [settings.otp_auth.phone_country],
                  allowDropdown: false,
                }
              }
              var iti = window.intlTelInput(field, intlTelInputConfig);
              // Listen to the telephone input for changes. And get the dialcode.
              field.addEventListener('countrychange', function(e) {
                if (iti.getSelectedCountryData().dialCode != undefined && !field.value.length) {
                  field_code.value = '+' + iti.getSelectedCountryData().dialCode;
                }
              });
            }
          });

        }
      }
    }
  };

})(Drupal, drupalSettings);
