(function($, Drupal) {
    Drupal.behaviors.otpAuth = {
        attach: function(context, settings) {
            if ($('html').hasClass("no-touch")) {
                $('input.form-text:first').focus();
            }

            var convertStoMs = function(seconds) {
                let minutes = Math.floor(seconds / 60);
                let extraSeconds = seconds % 60;
                minutes = minutes < 10 ? "0" + minutes : minutes;
                extraSeconds = extraSeconds < 10 ? "0" + extraSeconds : extraSeconds;
                return minutes + ":" + extraSeconds;
            }

            var waitTime = convertStoMs(settings.otp_auth.resend_wait_time);

            var startTimer = function(element) {
                var wait = true;
                $('.form-item-otp-code .description').append('<span class="timer left">'+ waitTime +'</span>');

                var interval = setInterval(function() {
                    var timer = waitTime.split(':');
                    var minutes = parseInt(timer[0], 10);
                    var seconds = parseInt(timer[1], 10);

                    if (minutes === 0 && seconds === 0) {
                        wait = false;
                        localStorage.setItem('otpAuthTtl', null);
                        $(element).css({'display': 'block'});
                        $('.timer').css({'display': 'none'}).parent().css({'visibility': 'visible'});
                        clearInterval(interval);
                        return;
                    }

                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    $('.timer').html(minutes + ':' + seconds);
                    waitTime = minutes + ':' + seconds;
                }, 1000);
            };

            var currentTtl = localStorage.getItem('otpAuthTtl');
            if (currentTtl == 'null' || currentTtl == null) {
                currentTtl = Date.now() + (settings.otp_auth.resend_wait_time * 1000);
                localStorage.setItem('otpAuthTtl', currentTtl);
            }

            var diff = (Date.now() > currentTtl) ? 0 : currentTtl - Date.now();
            waitTime = convertStoMs(Math.ceil(diff/1000))

            if ($('.user-retry-verification').length){
                if (waitTime == '0:0' || waitTime == '00:00') {
                    $('.user-retry-verification').css({'display': 'block'});
                    localStorage.setItem('otpAuthTtl', null);
                } else {
                    $('.form-item-otp-code .description').css({'display': 'block'});
                    startTimer($('.user-retry-verification'));
                }
            }
        }
    };
}(jQuery, Drupal));
